
# Symfony5-ddd-api

Simple Example Api Rest Baraja with Symfony 5.1 ddd(Domain-Driven Design) and Json Web Token

Author: Varuzhan Avetisyan
email: avarujan@gmail.com
phone: 631306840

Como instalar el project

1. git clone https://varuzhan-danny@bitbucket.org/varuzhan-danny/symfony5-ddd-api.git
2. cd symfony5-ddd-api
3. composer install
4. bin/console doctrine:database:create
5. bin/console doctrine:schema:update --force
6. bin/console doctrine:fixtures:load 
7. php -S 127.0.0.1:3005 -t public

Como usar:
Para obtener JWT token usa 
username : tester
password : password

Copiar el token y añadir en header como este format

authorization: Bearer {{token}}

Ver este video :  https://www.screencast.com/t/IuiuyRXeGHB
