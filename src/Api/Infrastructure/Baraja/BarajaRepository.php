<?php

declare(strict_types=1);

namespace App\Api\Infrastructure\Baraja;

use App\Api\Domain\Baraja\Entity\Baraja;
use App\Api\Domain\Baraja\Contract\BarajaRepositoryInterface;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\EntityManagerInterface;

class BarajaRepository extends ServiceEntityRepository implements BarajaRepositoryInterface
{
    private  $em;
    
    public function __construct(ManagerRegistry $registry, EntityManagerInterface $em)
    {
        $this->em = $em;
        parent::__construct($registry, Baraja::class);
    }
    
    public function add(Baraja $baraja): void
    {
        $this->em->persist($baraja);
        $this->em->flush();
    }
    
    public function findBarajaByFaccion(string $faccion): array
    {
         return $this->em->createQueryBuilder()
            ->select('b')
            ->from(Baraja::class, 'b')
            ->andWhere('b.faccion = :faccion')
            ->setParameters(['faccion' => $faccion])
            ->getQuery()->getResult();
    }
    
    
    public function isSatisfiedByNombreFaccion(string $nombre, string $faccion): bool
    {
        return $this->em->createQueryBuilder()
            ->select('b')
            ->from(Baraja::class, 'b')
            ->where('b.nombre = :nombre')
            ->andWhere('b.faccion = :faccion')
            ->setParameters(['nombre' => $nombre, 'faccion' => $faccion])
            ->getQuery()->getOneOrNullResult() === null;
    }
    
    public function getBarajas(): array
    {
        return $this->em->createQueryBuilder()
            ->select('bar')
            ->from(Baraja::class, 'bar')
            ->getQuery()->getResult();
    }
    
}
