<?php

declare(strict_types=1);

namespace App\Api\Infrastructure\User;

use App\Api\Domain\User\Entity\User;
use App\Api\Domain\User\Contract\UserRepositoryInterface;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

class UserRepository extends ServiceEntityRepository implements UserRepositoryInterface
{

    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, User::class);
    }
}