<?php

declare(strict_types=1);

namespace App\Api\Infrastructure\Carta;

use App\Api\Domain\Carta\Contract\CartaRepositoryInterface;
use App\Api\Domain\Carta\Entity\Carta;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

class CartaRepository extends ServiceEntityRepository implements CartaRepositoryInterface
{
    private  $em;

    public function __construct(ManagerRegistry $registry, EntityManagerInterface $em)
    {
        $this->em = $em;
         parent::__construct($registry, Carta::class);
    }
    

    public function add(Carta $carta): void
    {
        $this->em->persist($carta);
        $this->em->flush();
    }
    
}