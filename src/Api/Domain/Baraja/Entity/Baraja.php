<?php

declare(strict_types=1);

namespace App\Api\Domain\Baraja\Entity;

use Doctrine\ORM\Mapping as ORM;
use App\Api\Domain\Carta\Entity\Carta;

class Baraja implements \JsonSerializable
{
    const FACCION_BARBAROS = "barbaros";
    const FACCION_IMPERIALES = "imperiales";
    const FACCION_MONSTRUOS = "monstruos";
    const FACCION_NEATURAL = "neutral";
    const FACCION = [self::FACCION_BARBAROS, self::FACCION_IMPERIALES, self::FACCION_MONSTRUOS, self::FACCION_NEATURAL];
    
    
  /**
     * @var integer
     */
    protected $id;

    /**
     * @var string
     */
    protected $nombre;
    
    /**
     * @var string
     */
    protected $faccion;

    /**
     * @var Carta|ArrayCollection
     */
    protected $cartas;
    
    /**
     * @return integer
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getNombre(): string
    {
        return $this->nombre;
    }
    
    /**
     * @return string
     */
    public function getFaccion(): string
    {
        return $this->faccion;
    }
    
     /**
     * @return Carta|ArrayCollection
     */
    public function getCartas()
    {
        return $this->cartas;
    }
    
    /**
     * @param string $nombre
     */
    public function setNombre(string $nombre): void
    {
        $this->nombre = $nombre;
    }
    
     /**
     * @param string $faccion
     */
    public function setFaccion(string $faccion): void
    {
        if (!in_array($faccion, self::FACCION)) {
            throw new \InvalidArgumentException('faccion entre barbaros, imperiales, monstruos, neutral');
        }
        $this->faccion = $faccion;
    }
    
    /**
     * @param Carta $contributor
     */
    public function addCartas(Carta $carta)
    {
        if ($this->cartas->count($carta)< 20) {
            $this->cartas->add($carta);
        }
    }
    
     public function jsonSerialize():array
    {
        return [
            'id' => $this->getId(),
            'nombre' => $this->getNombre(),
            'faccion' => $this->getFaccion(),
            'cartas' => $this->getCartas()->toArray()
        ];
    }
    

}