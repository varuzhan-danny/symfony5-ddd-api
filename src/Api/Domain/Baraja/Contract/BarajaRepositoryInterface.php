<?php

declare(strict_types=1);

namespace App\Api\Domain\Baraja\Contract;

use App\Api\Domain\Baraja\Entity\Baraja;

interface BarajaRepositoryInterface
{

    public function add(Baraja $baraja): void;
    
    public function isSatisfiedByNombreFaccion(string $nombre, string $faccion): bool;
    
    public function findBarajaByFaccion(string $faccion): array;
    
    public function getBarajas(): array;
}
