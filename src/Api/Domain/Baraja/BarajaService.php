<?php

declare(strict_types=1);

namespace App\Api\Domain\Baraja;

use App\Api\Domain\Baraja\Entity\Baraja;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityNotFoundException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\RouterInterface;

class BarajaService
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;
    
    
     public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @param $id
     */
    public function getBarajaById($id)
    {
        $baraja = $this->entityManager->getRepository(Baraja::class)->find($id);
        if ($baraja) {
                    return $baraja;
        }

        throw new EntityNotFoundException("Baraja not found");
    }

    /**
     * @param Request $request
     */
    public function addBaraja(Request $request)
    {
        if(!$this->entityManager->getRepository(Baraja::class)->isSatisfiedByNombreFaccion($request->request->get('nombre'), $request->request->get('faccion'))){
          throw new \InvalidArgumentException('Data already exist.');
        }
        
        $baraja = new Baraja();
        $baraja->setNombre($request->request->get('nombre'));
        $baraja->setFaccion($request->request->get('faccion'));
        
        $this->entityManager->getRepository(Baraja::class)->add($baraja);
        
    }

}