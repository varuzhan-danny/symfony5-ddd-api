<?php

declare(strict_types=1);

namespace App\Api\Domain\User\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;

class User implements UserInterface
{
 /**
 * @var integer
  */
 private $id;
 /**
  * @var string
  */
 private $username;
 /**
  * @var string
  */
 private $password;

 /**
  * @var string
  */
 private $email;

 /**
  * @return string
  */
 public function getUsername()
 {
  return $this->username;
 }

 /**
  * @param mixed $username
  */
 public function setUsername($username): void
 {
  $this->username = $username;
 }

 /**
  * @return string|null
  */
 public function getSalt()
 {
  return null;
 }

 /**
  * @return string|null
  */
 public function getPassword()
 {
  return $this->password;
 }

 /**
  * @param $password
  */
 public function setPassword($password)
 {
  $this->password = $password;
 }
 /**
  * @return mixed
  */
 public function getEmail()
 {
  return $this->email;
 }

 /**
  * @param mixed $email
  */
 public function setEmail($email): void
 {
  $this->email = $email;
 }

 /**
  * @return array|string[]
  */
 public function getRoles()
 {
  return array('ROLE_USER');
 }

 public function eraseCredentials()
 {
 }
}