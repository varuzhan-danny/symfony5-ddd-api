<?php

declare(strict_types=1);

namespace App\Api\Domain\Carta\Contract;

use App\Api\Domain\Carta\Entity\Carta;

interface CartaRepositoryInterface
{

    public function add(Carta $carta): void;
    
}
