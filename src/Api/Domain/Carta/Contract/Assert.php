<?php

declare(strict_types=1);

namespace App\Api\Domain\Carta\Contract;

use Webmozart\Assert\Assert as WebmozartAssert;

final class Assert extends WebmozartAssert
{
    /**
     * @param string $message
     *
     * @throws InvalidArgumentException
     */
    protected static function reportInvalidArgument($message): void
    {
        throw new \InvalidArgumentException($message);
    }
}
