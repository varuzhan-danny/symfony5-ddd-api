<?php

declare(strict_types=1);

namespace App\Api\Domain\Carta;

use App\Api\Domain\Carta\Entity\Carta;
use App\Api\Domain\Baraja\Entity\Baraja;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityNotFoundException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\RouterInterface;

class CartaService
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;
    
    
     public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @param Request $request
     */
    public function addCarta(Request $request)
    {
        if(!$request->request->get('nombre') || !$request->request->get('coste') || !$request->request->get('poder') || !$request->request->get('faccion')){
          throw new \InvalidArgumentException('Type all data.');
        }
        $barajaRepository = $this->entityManager->getRepository(Baraja::class);
        $carta = new Carta();
        $carta->setNombre($request->request->get('nombre'));
        $carta->setCoste($request->request->get('coste'));
        $carta->setPoder($request->request->get('poder'));
        $carta->setFaccion($request->request->get('faccion'));
        
        if($request->request->get('faccion') == 'neutral'){
            $barajas = $barajaRepository->getBarajas();
            
            if($barajas){
                foreach($barajas as $bar){
                    $bar->addCartas($carta);
                }
            }
        }else{
             $barajas = $barajaRepository->findBarajaByFaccion($request->request->get('faccion')); 
             
             if($barajas){
                  foreach($barajas as $bar){
                    $bar->addCartas($carta);
                }
             }
        }
        
        $this->entityManager->getRepository(Carta::class)->add($carta);
        
    }

}