<?php

declare(strict_types=1);

namespace App\Api\Domain\Carta\Entity;

use App\Api\Domain\Carta\Contract\Assert as Assert;
use Doctrine\ORM\Mapping as ORM;

class Carta implements \JsonSerializable
{
    
    const FACCION_BARBAROS = "barbaros";
    const FACCION_IMPERIALES = "imperiales";
    const FACCION_MONSTRUOS = "monstruos";
    const FACCION_NEATURAL = "neutral";
    const FACCION = [self::FACCION_BARBAROS, self::FACCION_IMPERIALES, self::FACCION_MONSTRUOS, self::FACCION_NEATURAL];
    

    /**
     * @var integer
     */
    protected $id;

    /**
     * @var string
     */
    protected $nombre;

     /**
     * @var integer
     */
    protected $coste;

    /**
     * @var integer
     */
    protected $poder;

    /**
     * @var string
     */
    protected $faccion;

    /**
     * @return integer
     */
    public function getId(): int {
        return $this->id;
    }
    
    /**
     * @return string
     */
    public function getNombre(): string
    {
        return $this->nombre;
    }
    
    /**
     * @return string
     */
    public function getFaccion(): string
    {
        return $this->faccion;
    }
    
    /**
     * @return integer
     */
    public function getCoste(): int
    {
        return $this->coste;
    }
    
    /**
     * @return integer
     */
    public function getPoder(): int
    {
        return $this->poder;
    }

    /**
     * @param string $nombre
     */
    public function setNombre(string $nombre): void 
    {
        $this->nombre = $nombre;
    }
    
    /**
     * @param integer $cost
     */
    public function setCoste(int $cost): void 
    {
        
        Assert::range($cost, 4, 14, 'Coste entre %2$s y %3$s. Sale: %s');
        
        $this->coste = $cost;
    }
    
    /**
     * @param integer $poder
     */
    public function setPoder(int $poder): void 
    {
        Assert::range($poder, 1, 12, 'Poder entre %2$s y %3$s. Sale: %s');
        
        $this->poder = $poder;
    }

    /**
     * @param string $faccion
     */
    public function setFaccion(string $faccion): void
    {
        if (!in_array($faccion, self::FACCION)) {
            throw new \InvalidArgumentException('faccion entre barbaros, imperiales, monstruos, neutral');
        }
        $this->faccion = $faccion;
    }
    
    public function jsonSerialize():array
    {
        return [
            'id' => $this->getId(),
            'nombre' => $this->getNombre(),
            'faccion' => $this->getFaccion(),
            'coste' => $this->getCoste(),
            'poder' => $this->getPoder()
        ];
    }
    

}
