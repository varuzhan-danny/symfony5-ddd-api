<?php

declare(strict_types=1);

namespace App\Api\Http\Controller;

use App\Api\Domain\Carta\CartaService;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\RouterInterface;
use Swagger\Annotations as SWG;

class CartaController
{

    /**
     * @param Request $request
     * @return JsonResponse
     * @SWG\Response(
     *     response=200,
     *     description="returns success message"
     * )
     * @SWG\Response(
     *     response=500,
     *     description="Returns error"
     * )
     *
     *  @SWG\Parameter(
     *          name="Carta",
     *          in="body",
     *          description="Añadir Carts los parametros nombre, faccion(entre barbaros, imperiales, monstruos, neutral), coste, poder",
     *          required=true,
     *          format="application/json",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(property="nombre", type="string"),
     *              @SWG\Property(property="faccion", type="string"),
     *              @SWG\Property(property="coste", type="integer"),
     *              @SWG\Property(property="poder", type="integer")
     *          )
     * )
     * @SWG\Tag(name="Carta")
     */
    public function add(Request $request, CartaService $cartaService):JsonResponse
    {
        try {
            
            $cartaService->addCarta($request);
            
            return new JsonResponse('Carta has been added', Response::HTTP_OK);
        }catch (\Exception $exception) {
            return new JsonResponse($exception->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }

    }

}