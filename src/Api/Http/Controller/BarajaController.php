<?php

declare(strict_types=1);

namespace App\Api\Http\Controller;

use App\Api\Domain\Baraja\BarajaService;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\RouterInterface;
use Swagger\Annotations as SWG;

class BarajaController
{

    /**
     * @param $id
     * @return JsonResponse
     * @SWG\Response(
     *     response=200,
     *     description="Returns Baraja"
     * )
     * @SWG\Response(
     *     response=500,
     *     description="Returns error"
     * )
     * 
     * @SWG\Tag(name="Baraja")
     */
    public function baraja(BarajaService $barajaService, $id): JsonResponse
    {
         
        try {
           $baraja = $barajaService->getBarajaById($id);
           
            if (!$id){
                    throw new \Exception();
            }
            
            return new JsonResponse($baraja,  Response::HTTP_OK);
        }catch (\Exception $e) {
            return new JsonResponse('Datos no encontrados', 422);
        }
    }

    /**
     * @param Request $request
     * @return JsonResponse
     * @SWG\Response(
     *     response=200,
     *     description="Returns true"
     * )
     * @SWG\Response(
     *     response=500,
     *     description="Returns error"
     * )
     * 
     *  @SWG\Parameter(
     *          name="Baraja",
     *          in="body",
     *          description="Añadir Baraja los parametros nombre, faccion(entre barbaros, imperiales, monstruos, neutral)",
     *          required=true,
     *          format="application/json",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(property="nombre", type="string"),
     *              @SWG\Property(property="faccion", type="string")
     *          )
     *

     * 
     * )
     * @SWG\Tag(name="Baraja")
     */
    public function add(Request $request, BarajaService $barajaService):JsonResponse
    {
        try {
           
            $barajaService->addBaraja($request);
            return new JsonResponse('Baraja ha sido añadido', Response::HTTP_OK);
        }catch (\Exception $exception) {
            return new JsonResponse($exception->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }

    }

}