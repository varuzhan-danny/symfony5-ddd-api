<?php

declare(strict_types=1);

namespace App\Authorization\Controller;

use App\Entity\User;
use Lexik\Bundle\JWTAuthenticationBundle\Services\JWTTokenManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Nelmio\ApiDocBundle\Annotation\Operation;
use Swagger\Annotations as SWG;

/**
 * Class AuthController
 *
 * {@inheritdoc}
 */

class AuthController extends AbstractController
{

    /**
     * @Operation(
     *     tags={"Json Web Token"},
     *     summary="Authorize user",
     *     *  @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Use username:tester, password:password",
     *          required=true,
     *          format="application/json",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(property="username", type="string"),
     *              @SWG\Property(property="password", type="string")
     *          )
     * ),
     *     @SWG\Response(
     *         response="200",
     *         description="Returned when successful"
     *     )
     * )
     */
    
    public function getToken(Request $request, UserInterface $user, JWTTokenManagerInterface $JWTManager): JsonResponse
    {
       return new JsonResponse(['token' => $JWTManager->create($user)]);
    }
}
