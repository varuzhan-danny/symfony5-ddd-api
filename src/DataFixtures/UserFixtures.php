<?php

declare(strict_types=1);

namespace App\DataFixtures;

use App\Api\Domain\User\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserFixtures extends Fixture implements OrderedFixtureInterface
{
    
    private $passwordEncoder;

    public function __construct(UserPasswordEncoderInterface $passwordEncoder)
    {
        $this->passwordEncoder = $passwordEncoder;
    }
    
    public function load(ObjectManager $manager): void
    {
        $userAdmin = new User();
        $userAdmin->setUsername('tester');
        $userAdmin->setEmail('tester@symfony.com');
        $encodedPassword = $this->passwordEncoder->encodePassword($userAdmin, 'password');
        $userAdmin->setPassword($encodedPassword);
        $manager->persist($userAdmin);

        $manager->flush();
    }
    
    /**
     * Get the order of this fixture
     *
     * @return integer
     */
    public function getOrder():int
    {
        return 1;
    }

}
